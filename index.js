const fs = require('fs');
var buffer = Buffer.alloc(1722);
var PNB_History = [];
const http = require('http');

fs.stat('PNB_share_Details.csv', function(err, stats) {
    if (err) {
        return console.error(err);
    }
    console.log("isFile ? " + stats.isFile());
    console.log("isDirectory ? " + stats.isDirectory());
});

fs.open('PNB_share_Details.csv', 'r+', function(err, fd) {
    if (err) {
        return console.error(err);
    }
    console.log("File opened successfully!");
    console.log("Going to read the file");
    fs.read(fd, buffer, 0, buffer.length, 0, function(err, bytes) {
        if (err) {
            console.log(err);
        }
        console.log(bytes + " bytes read");
        if (bytes > 0) {
            var bufferString = buffer.toString()
            for (let i = 0; i < bufferString.length; i++) {
                bufferString = bufferString.replace('\n', ',').trim();
            }
            var makeArray = bufferString.split(',');
            for (var index = 0; index < makeArray.length; index += 7) {
                PNB_History.push({
                    'Date': makeArray[index],
                    'Open': makeArray[index + 1],
                    'High': makeArray[index + 2],
                    'Low': makeArray[index + 3],
                    'Close': makeArray[index + 4],
                    'Adjust Close': makeArray[index + 5],
                    'Volume': makeArray[index + 6]
                });
            }
            console.log('Now buffer has array object of PNB shares. Now write it in a File');
            fs.writeFile('pnbShare.json', JSON.stringify(PNB_History), function(err) {
                if (err) {
                    return console.error(err);
                }
                console.log("Data written successfully! Now create server and type localhost:8080");
                http.createServer(function(request, response) {
                    response.writeHead(200, { 'Content-Type': 'text/html' });
                    response.write(JSON.stringify(PNB_History));
                    response.end();
                }).listen(8080);
            });
        }
    });
});